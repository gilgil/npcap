npcap 설치
====

## 2019.05.16
현재() npcap 홈페이지([npcap.org](https://npcap.org)) 를 가면 죄선 버전은 다음과 같다.
* Npcap 0.995 installer ( npcap-0.995.exe )
* Npcap SDK 1.02 (ZIP) ( npcap-sdk-1.02.zip )

그런데 npcap-sdk-1.02.zip 파일 안에는 lib 파일이 없어 사용하지 못한다(링크를 하지 못한다).  
그래서 그 이전 버전인 npcap-sdk-1.01.zip 파일을 사용해서 개발을 한다.  
모든 zip 파일은 [npcap.org/dist](https://npcap.org/dist) 에 archivng되어 있다.  

## 2020.10.13
* Npcap 1.00 installer( npcap-1.00.exe ) 추가
* Npcap SDK 1.06 ( npcap-sdk-1.06.zip ) 추가
* 파일을 npcap-sdk-1.06.zip을 이용하여 git 파일 업데이트

## 2021.10.11
* Npcap 1.55 installer( npcap-1.55.exe ) 추가
* Npcap SDK 1.11( npcap-sdk-1.11.zip ) 추가
* 파일을 npcap-sdk-1.11.zip을 이용하여 git 파일 업데이트